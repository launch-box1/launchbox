"use client";
import React, { useState } from "react";
import { motion } from "framer-motion";
import Image from "next/image";

const Index = () => {
  const [toggle, setToggle] = useState(false);

  const handleClick = () => {
    setToggle(!toggle);
  };

  const textVariants = {
    hidden: { opacity: 0, y: 50 },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 0.5,
        ease: "easeOut",
      },
    },
  };

  return (
    <>
      <div className="">
        <p className="text-4xl text-center lg:text-5xl pt-4 font-semibold sm:leading-tight mt-5 text-center min-[360px]:text-3xl">
          About Us
        </p>
        <p className="text-lg pt-4 font-normal lh-33 text-center text-black">
          Unleashing the Potential of Exceptional Online Services
        </p>
      </div>
      <div
        className="w-full flex h-2/3 pb-16 mb-16 max-[420px]:block max-[420px]:pr-12 max-[420px]:pl-12 max-[420px]:mt-12"
        id="about"
      >
        <div className="flex place-content-end w-1/2 pr-36 max-[420px]:pr-0 max-[420px]:w-full max-[420px]:block">
          <span className="mt-24">
            <Image src="/assets/aboutbg.svg" height={500} width={500} alt="" />
          </span>
        </div>
        <div className="w-1/2 pr-80 mt-28 max-[420px]:w-full max-[420px]:pr-0 max-[420px]:mt-12">
          <motion.h4
            className="text-lg pt-4 font-normal lh-33 text-center lg:text-start text-bluegray"
            variants={textVariants}
            initial="hidden"
            animate={toggle ? "visible" : "hidden"}
          >
            At Launchbox, our journey began with a powerful mission in mind: to
            revolutionize the digital landscape and empower businesses to thrive
            in the digital age. We hold a strong belief that technology has the
            potential to revolutionize businesses of all sizes and industries,
            empowering them to thrive and succeed. Our founders have come
            together with a shared vision to bridge the gap between cutting-edge
            technology and businesses seeking to make their mark in the digital
            realm. <br />
            <br />
            <span
              className="font-semibold text-[#229680] cursor-pointer"
              onClick={handleClick}
            >
              Less Info ...
            </span>
          </motion.h4>
          <motion.h4
            className="text-lg -mt-[270px] pt-4 font-normal lh-33 text-center lg:text-start text-bluegray"
            variants={textVariants}
            initial="hidden"
            animate={!toggle ? "visible" : "hidden"}
          >
            At Launchbox, our journey began with a powerful mission in mind: to
            revolutionize the digital landscape and empower businesses to thrive
            in the digital age. We hold a strong belief that technology has the
            potential to revolutionize.... <br />
            <br />
            <span
              className="font-semibold text-[#229680] cursor-pointer relative z-20"
              onClick={handleClick}
            >
              More Info ...
            </span>
          </motion.h4>
        </div>
      </div>
    </>
  );
};

export default Index;
